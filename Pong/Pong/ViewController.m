//
//  ViewController.m
//  Pong
//
//  Created by Rohit Singh on 01/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    AnimatedBackGround.animationImages=[NSArray arrayWithObjects: [UIImage imageNamed:@"Desktop1.png"],
                                                                [UIImage imageNamed:@"Desktop2.png"],
                                        [UIImage imageNamed:@"Desktop3.png"],
                                        [UIImage imageNamed:@"Desktop4.png"],
                                        [UIImage imageNamed:@"Desktop5.png"],nil
                                        
                                        ];
    [AnimatedBackGround setAnimationRepeatCount:0];
    [AnimatedBackGround setAnimationDuration:5];
    [AnimatedBackGround startAnimating];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
