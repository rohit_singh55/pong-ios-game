//
//  GameViewController.h
//  Pong
//
//  Created by Rohit Singh on 01/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
int X;
int Y;
int ComputerScoreNumber;
int PlayerScoreNumber;
@interface GameViewController : UIViewController
{
    __weak IBOutlet UIImageView *Computer;
    __weak IBOutlet UILabel *winorloss;
    __weak IBOutlet UIButton *Exit;
    __weak IBOutlet UILabel *playerscorelabel;
    __weak IBOutlet UILabel *comouterscorelabel;
IBOutlet UIImageView *Ball;
    __weak IBOutlet UIImageView *Player;
IBOutlet UIButton *StartButton;
NSTimer *timer;
    SystemSoundID PlaySoundID;
}

-(IBAction)startButton:(id)sender;
-(void)balllMOvement;
-(void)ComputerMovement;
-(void)collision;
@end
