//
//  GameViewController.m
//  Pong
//
//  Created by Rohit Singh on 01/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "GameViewController.h"
#import "AVFoundation/AVAudioPlayer.h"
#import <AudioToolbox/AudioToolbox.h>
@interface GameViewController ()

@end

@implementation GameViewController

-(IBAction)startButton:(id)sender{
    StartButton.hidden=YES;
    Exit.hidden=YES;
    Y=arc4random()%11;
    Y=Y-5;//Y -5 to 5
    X=arc4random()%11;
    X=X-5;   //X -5 to 5
    if(Y==0){//no movement vertcially
        Y=1;
    }if(X==0){
        X=1;
    
    }
    NSLog(@"clicked");
    timer=[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(balllMOvement) userInfo:nil repeats:YES];

}
-(void)collision{
    if (CGRectIntersectsRect(Ball.frame, Player.frame)) {
        Y=arc4random()%5;
        Y=Y-5;
        [self playSound];
    }
    if (CGRectIntersectsRect(Ball.frame, Computer.frame)) {
        Y=arc4random()%5;
        
    }


}
-(void)balllMOvement{
    [self ComputerMovement];
    [self collision];
    Ball.center=CGPointMake(Ball.center.x+X, Ball.center.y+Y);
    if (Ball.center.x<15) {
        X=0-X;
    }
    if (Ball.center.x>305) {
        X=0-X;
    }
    if(Ball.center.y<0){
        PlayerScoreNumber=PlayerScoreNumber+1;
        playerscorelabel.text=[NSString stringWithFormat:@"%i",PlayerScoreNumber];
        [timer invalidate];
        StartButton.hidden=NO;
        Ball.center=CGPointMake(142, 238);
        
    if (PlayerScoreNumber==5) {
        StartButton.hidden = YES;
        Exit.hidden=NO;
        winorloss.hidden=NO;
        winorloss.text=[NSString stringWithFormat:@"You Win!!"];
    }
        
    }
    if(Ball.center.y>542){
        ComputerScoreNumber=ComputerScoreNumber+1;
        comouterscorelabel.text=[NSString stringWithFormat:@"%i",ComputerScoreNumber];
        [timer invalidate];
        StartButton.hidden=NO;
        Ball.center=CGPointMake(142, 238);
        Computer.center=CGPointMake(92, 26);
        if (ComputerScoreNumber==5) {
            StartButton.hidden = YES;
            Exit.hidden=NO;
            winorloss.hidden=NO;
            winorloss.text=[NSString stringWithFormat:@"You Loss"];
        }
        
    }
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *Drag=[[event allTouches]anyObject];
    Player.center=[Drag locationInView:self.view];
    if(Player.center.y>511){
        Player.center=CGPointMake(Player.center.x, 511);
    }
    if(Player.center.y<511){
        Player.center=CGPointMake(Player.center.x, 511);
    }
    if (Player.center.x<0) {
        Player.center=CGPointMake(0, Player.center.y);
    }
    if (Player.center.x>283) {
        Player.center=CGPointMake(283, Player.center.y);
    }


}
-(void)ComputerMovement{

    if(Computer.center.x>Ball.center.x){
        Computer.center=CGPointMake(Computer.center.x-2, Computer.center.y);
    
    }
    if(Computer.center.x<Ball.center.x){
    Computer.center=CGPointMake(Computer.center.x+2, Computer.center.y);
    
    }
    if (Computer.center.x<0) {
        Computer.center=CGPointMake(0, Computer.center.y);
    }
    if (Computer.center.x>283) {
        Computer.center=CGPointMake(283, Computer.center.y);
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Player.center=CGPointMake(99, Player.center.y);
    PlayerScoreNumber=0;
    ComputerScoreNumber=0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) playSound {
    NSURL *SoundURl=[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"tick" ofType:@"mp3"]];
                     AudioServicesCreateSystemSoundID((__bridge CFURLRef)SoundURl, &PlaySoundID);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
